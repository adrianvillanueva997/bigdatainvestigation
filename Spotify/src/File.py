import json
import os
import nltk
import pandas as pd


class Data_Extraction():
    def __init__(self, path):
        self.path = path
        self.data = {
            'Songs': [],
            'Artists': [],
            'Playlist_Name': []
        }

    def Get_Path(self):
        for root, dirs, files in os.walk(self.path):
            for file in files:
                if file.endswith(".json"):
                    json_file = (os.path.join(root, file))
                    print(json_file)
                    self.Read_File(json_file)

    def Read_File(self, path):
        with open(path, 'r', encoding='utf-8') as file:
            file_readed = file.read()
            jsonFile = json.loads(file_readed)
            for playlist in jsonFile['playlists']:
                self.data['Playlist_Name'].append(playlist['name'])
                for song in playlist['tracks']:
                    self.data['Songs'].append(song['track_name'])
                    #self.artists.append(song['artist_name'])
            # print('Playlists: ' + str(len(self.data['Playlist_Name'])))
            print('Songs: ' + str(len(self.data['Songs'])))
            # print('Artists: ' + str(len(self.artists)))
            # df['Artists'] = artists
            # df['Songs'] = songs

    def Tokenizer(self, playlist):
        print(nltk.word_tokenize(playlist))

    def getDataframe(self):
        df = pd.DataFrame()
        df['Artists'] = self.data['Artists']
        df['Songs'] = self.data['Songs']
        df.drop_duplicates('first', True)
        return df
