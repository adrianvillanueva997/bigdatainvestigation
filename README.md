# bigdatainvestigation

## Getting Started

This repository aims to provide an atempt to investigate a big data issue known as the [cold start](https://en.wikipedia.org/wiki/Cold_start) using an amazon database.

### Prerequisites

Make sure to have python 3 installed in your computer, if not you can download it from [here](https://www.python.org). Since pip comes by default in python 3 there is no need to install it.

If you are using any Linux distro with apt package management just type the following in the terminal.

```
sudo apt-get update
sudo apt-get install python3.6
sudo apt-get install python3-pip
```

To install the needed libraries use the following commands, they work for Windows and Linux. For windows users, you have to use the CMD terminal to install them.

```
pip3 install pandas

```


## Built With

* [Python 3](https://www.python.org) 
* [Pandas](https://pandas.pydata.org/)
* [json](https://www.json.org/json-es.html)


## Authors

* **Adrián Villanueva Martínez** -  [thexiao77](https://github.com/thexiao77)
* **Daniel Vilar Seoane** - [40CompilerErrors](https://github.com/40CompilerErrors)
* **Germán García García** - [3Gsman](https://github.com/3Gsman)

## License

This project is licensed under GPL V3 - see the [LICENSE.md](https://github.com/thexiao77/bigdatainvestigation/blob/master/LICENSE) file for details



