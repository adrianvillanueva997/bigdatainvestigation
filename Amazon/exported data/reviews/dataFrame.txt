             asin   helpful  overall  \
0      1384719342    [0, 0]      5.0   
1      1384719342  [13, 14]      5.0   
2      1384719342    [1, 1]      5.0   
3      1384719342    [0, 0]      5.0   
4      1384719342    [0, 0]      5.0   
5      B00004Y2UT    [0, 0]      5.0   
6      B00004Y2UT    [0, 0]      5.0   
7      B00004Y2UT    [0, 0]      3.0   
8      B00004Y2UT    [0, 0]      5.0   
9      B00004Y2UT    [0, 0]      5.0   
10     B00004Y2UT    [6, 6]      5.0   
11     B00005ML71    [0, 0]      4.0   
12     B00005ML71    [0, 0]      3.0   
13     B00005ML71    [0, 0]      5.0   
14     B00005ML71    [0, 0]      5.0   
15     B00005ML71    [0, 0]      2.0   
16     B000068NSX    [0, 0]      4.0   
17     B000068NSX    [0, 0]      5.0   
18     B000068NSX    [3, 3]      5.0   
19     B000068NSX    [0, 0]      5.0   
20     B000068NSX    [0, 0]      4.0   
21     B000068NSX    [0, 0]      5.0   
22     B000068NSX    [0, 0]      4.0   
23     B000068NTU    [0, 0]      5.0   
24     B000068NTU    [0, 0]      5.0   
25     B000068NTU    [0, 0]      4.0   
26     B000068NTU    [0, 0]      5.0   
27     B000068NTU    [0, 0]      5.0   
28     B000068NVI    [1, 1]      4.0   
29     B000068NVI    [0, 0]      5.0   
...           ...       ...      ...   
10231  B00IZCSW3M    [1, 1]      5.0   
10232  B00IZCSW3M    [0, 0]      5.0   
10233  B00IZCSW3M    [7, 7]      5.0   
10234  B00IZCSW3M    [1, 1]      5.0   
10235  B00IZCSW3M    [1, 2]      5.0   
10236  B00IZCSW3M    [0, 0]      5.0   
10237  B00IZCSW3M   [8, 10]      3.0   
10238  B00IZCSW3M    [0, 0]      5.0   
10239  B00IZCSW3M    [2, 2]      5.0   
10240  B00J4TBMVO    [0, 0]      5.0   
10241  B00J4TBMVO    [0, 0]      5.0   
10242  B00J4TBMVO    [0, 0]      5.0   
10243  B00J4TBMVO    [0, 0]      5.0   
10244  B00J4TBMVO    [0, 0]      5.0   
10245  B00J4TBMVO    [0, 0]      4.0   
10246  B00J4TBMVO    [0, 0]      5.0   
10247  B00J4TBMVO    [2, 2]      2.0   
10248  B00JBIVXGC    [0, 0]      5.0   
10249  B00JBIVXGC    [0, 0]      5.0   
10250  B00JBIVXGC    [0, 0]      5.0   
10251  B00JBIVXGC    [0, 0]      5.0   
10252  B00JBIVXGC    [0, 0]      5.0   
10253  B00JBIVXGC    [0, 0]      4.0   
10254  B00JBIVXGC    [0, 0]      5.0   
10255  B00JBIVXGC    [0, 0]      3.0   
10256  B00JBIVXGC    [0, 0]      5.0   
10257  B00JBIVXGC    [0, 0]      5.0   
10258  B00JBIVXGC    [0, 0]      4.0   
10259  B00JBIVXGC    [0, 0]      4.0   
10260  B00JBIVXGC    [0, 0]      4.0   

                                              reviewText   reviewTime  \
0      Not much to write about here, but it does exac...  02 28, 2014   
1      The product does exactly as it should and is q...  03 16, 2013   
2      The primary job of this device is to block the...  08 28, 2013   
3      Nice windscreen protects my MXL mic and preven...  02 14, 2014   
4      This pop filter is great. It looks and perform...  02 21, 2014   
5      So good that I bought another one.  Love the h...  12 21, 2012   
6      I have used monster cables for years, and with...  01 19, 2014   
7      I now use this cable to run from the output of...  11 16, 2012   
8      Perfect for my Epiphone Sheraton II.  Monster ...   07 6, 2008   
9      Monster makes the best cables and a lifetime w...   01 8, 2014   
10     Monster makes a wide array of cables, includin...  04 19, 2012   
11     I got it to have it if I needed it. I have fou...  04 22, 2014   
12     If you are not use to using a large sustaining...  11 17, 2013   
13     I love it, I used this for my Yamaha ypt-230 a...  06 16, 2013   
14     I bought this to use in my home studio to cont...  12 31, 2012   
15     I bought this to use with my keyboard. I wasn'...  08 17, 2013   
16     This Fender cable is the perfect length for me...  08 13, 2013   
17     wanted it just on looks alone...It is a nice l...   07 9, 2013   
18     I've been using these cables for more than 4 m...  03 18, 2013   
19     Fender cords look great and work just as well....   08 7, 2013   
20     This is a cool looking cheap cable which works...  03 16, 2012   
21     The Fender 18 Feet California Clear Instrument...  01 29, 2014   
22     Very good cable. Well made and it looks great ...   12 8, 2012   
23     Got this cable to run a rockband keyboard cont...   07 5, 2012   
24     When I was searching for MIDI cables for my AR...  10 31, 2013   
25     Cant go wrong. Great quality on a budget price...   07 3, 2013   
26     The ends of the midi cable look and feel like ...  01 25, 2012   
27     Just trying to find a midi to midi was a task,...  10 15, 2013   
28     The Hosa XLR cables are affordable and very he...  07 11, 2012   
29     I bought these to go from my board to the amp....  02 19, 2014   
...                                                  ...          ...   
10231  For a long time, I never thought much about gu...   05 1, 2014   
10232  My father is a full-time gigging musician prim...  06 14, 2014   
10233  D'Addario has always been one of the best stri...  05 22, 2014   
10234  I usually use Gibson Vintage Nickle Strings on...  05 11, 2014   
10235  Excellent tone, and I'm a bit surprised by tha...  05 31, 2014   
10236  I've been stringing my guitars with D'Addario ...  05 25, 2014   
10237  Don't get me wrong, the improvement over the o...  04 28, 2014   
10238  These have a nice bright sound and are easy on...  06 18, 2014   
10239  D'Addario's NYXL1046 Nickel Plated Electric Gu...  06 17, 2014   
10240  Just put these on my Martin DCX1E and they sou...  07 11, 2014   
10241  I ordered this for my husband, the musician. H...  06 25, 2014   
10242  I had used Elixer strings for several years on...  07 10, 2014   
10243  I can't speak on the claim for a longer string...  07 12, 2014   
10244  Great product with nice feel and tone. I'm onl...  06 26, 2014   
10245  I have reviewed these strings before, and fran...   07 1, 2014   
10246  I had the opportunity to try a set of Elixir N...  07 19, 2014   
10247  I was looking forward to trying these, as I've...  06 24, 2014   
10248  I put these strings on my Guild acoustic elect...  07 11, 2014   
10249  I ordered this for my husband, the musician.  ...  06 22, 2014   
10250  I had used Elixer strings for several years on...  07 10, 2014   
10251  True to phosphor bronze these strings have a m...   07 1, 2014   
10252  I've used Elixirs for about five years now. Th...  06 30, 2014   
10253  I'm a D'Addario man myself, but hey free is fr...  07 21, 2014   
10254  I really like these strings.  While they are n...   07 4, 2014   
10255  I have lots of friends who play these strings....  06 25, 2014   
10256            Great, just as expected.  Thank to all.  07 20, 2014   
10257  I've been thinking about trying the Nanoweb st...   07 2, 2014   
10258  I have tried coated strings in the past ( incl...  07 22, 2014   
10259  Well, MADE by Elixir and DEVELOPED with Taylor...   07 1, 2014   
10260  These strings are really quite good, but I wou...  07 16, 2014   

           reviewerID                                      reviewerName  \
0      A2IBPI20UZIR0U  cassandra tu "Yeah, well, that's just like, u...   
1      A14VAT5EAX3D9S                                              Jake   
2      A195EZSQDW3E21                     Rick Bennette "Rick Bennette"   
3      A2C00NNG1ZQQG2                         RustyBill "Sunday Rocker"   
4       A94QU4C90B1AX                                     SEAN MASLANKA   
5      A2A039TZMZHH9Y                               Bill Lewey "blewey"   
6      A1UPZM995ZAH90                                             Brian   
7       AJNFQI3YR6XJ5                                 Fender Guy "Rick"   
8      A3M1PLEYNDEYO8                                   G. Thomas "Tom"   
9       AMNTZU1YQN1TH                                       Kurt Robair   
10     A2NYK9KWFMJV4Y                       Mike Tarrani "Jazz Drummer"   
11     A35QFQI0M46LWO                                     Christopher C   
12     A2NIT6BKW11XJQ                                               Jai   
13     A1C0O09LOLVI39                                           Michael   
14     A17SLR18TUMULM                                       Straydogger   
15     A2PD27UKAD3Q00              Wilhelmina Zeitgeist "coolartsybabe"   
16      AKSFZ4G1AXYFC                                      C.E. "Frank"   
17      A67OJZLHBBUQ9                  Charles F. Marks "charlie marks"   
18     A2EZWZ8MBEDOLN                                            Charlo   
19     A1CL807EOUPVP1                                           GunHawk   
20     A1GMWTGXW682GB                                          MetalFan   
21     A2G12DY50U700V                                       Ricky Shows   
22      A3E0CF25A7LD2                                            WBowie   
23     A2W3CLAYZLDPTV                         Amazon Customer "=Chris="   
24     A398X9POBHK69N                                   Ann Vande Zande   
25      AXWB93VKVML6K                                    Michael Hassey   
26     A2FZ4Z0UFA1OR8                                               Pat   
27      AXP9CF1UTFRSU                                              tada   
28     A2CCGGDGZ694CT                                          b carney   
29     A27DR1VO079F1V                                         Dan Edman   
...               ...                                               ...   
10231  A31RULW0KNYJ5H                                                LA   
10232  A3KZEGBTPH6MMF                                  Lucy Cat "Mandy"   
10233   AWCJ12KBO5VII                                  Michael L. Knapp   
10234  A29B4PAIOL7HYG                          N. Caruso "gibsonjunkie"   
10235  A27L5L6I7OSV5B                                      Otto Correct   
10236   AOMEH9W6LHC4S                                          Personne   
10237  A3VDSGNIS92OVZ                               P. Hamm "p-squared"   
10238  A27H0T39U3FZB5                        P. MSakamoto "boy clothes"   
10239  A2PD27UKAD3Q00              Wilhelmina Zeitgeist "coolartsybabe"   
10240  A146H4KN4LFR60                                         angelfood   
10241  A3A7Y3TSPPZU9T                                       coffeebrain   
10242  A30J7WQV0ZNRXG                         D. Reinstein "marindavid"   
10243  A3M1PLEYNDEYO8                                   G. Thomas "Tom"   
10244  A3KX8SVSUCSHKU                                              john   
10245  A1TSKKBNV38E8Y                       K. Harriger "K.R. Harriger"   
10246  A29B4PAIOL7HYG                          N. Caruso "gibsonjunkie"   
10247  A3VDSGNIS92OVZ                               P. Hamm "p-squared"   
10248  A146H4KN4LFR60                                         angelfood   
10249  A3A7Y3TSPPZU9T                                       coffeebrain   
10250  A30J7WQV0ZNRXG                         D. Reinstein "marindavid"   
10251  A3M1PLEYNDEYO8                                   G. Thomas "Tom"   
10252  A1SD1C8XK3Z3V1                   guitfiddleblue "guitfiddleblue"   
10253  A2VRAT69JDAD3W                        Jason Whitt "Whittmeister"   
10254  A306NASGVUDFKF                        Jeffrey E "jeffinaustintx"   
10255  A1TSKKBNV38E8Y                       K. Harriger "K.R. Harriger"   
10256  A14B2YH83ZXMPP                                   Lonnie M. Adams   
10257   A1RPTVW5VEOSI                                Michael J. Edelman   
10258   AWCJ12KBO5VII                                  Michael L. Knapp   
10259  A2Z7S8B5U4PAKJ                           Rick Langdon "Scriptor"   
10260  A2WA8TDCTGUADI                                   TheTerrorBeyond   

                                                 summary  unixReviewTime  
0                                                   good      1393545600  
1                                                   Jake      1363392000  
2                                   It Does The Job Well      1377648000  
3                          GOOD WINDSCREEN FOR THE MONEY      1392336000  
4                  No more pops when I record my vocals.      1392940800  
5                                         The Best Cable      1356048000  
6            Monster Standard 100 - 21' Instrument Cable      1390089600  
7                     Didn't fit my 1996 Fender Strat...      1353024000  
8                                            Great cable      1215302400  
9                   Best Instrument Cables On The Market      1389139200  
10     One of the best instrument cables within the b...      1334793600  
11                   It works great but I hardly use it.      1398124800  
12                            HAS TO GET USE TO THE SIZE      1384646400  
13                                               awesome      1371340800  
14                                             It works!      1356912000  
15          Definitely Not For The Seasoned Piano Player      1376697600  
16                              Durable Instrument Cable      1376352000  
17                           fender 18 ft. Cali clear...      1373328000  
18     So far so good.  Will revisit at the 6 month m...      1363564800  
19                Add California to the name and I jump!      1375833600  
20                   Cheap and cool looking, good length      1331856000  
21     Fender 18 Feet California Clear Instrument Cab...      1390953600  
22                                          Guitar Cable      1354924800  
23                                        Quality cable!      1341446400  
24     I Got Great Pricing, But Still a Really Good P...      1383177600  
25                                            Its a Hosa      1372809600  
26                                    Quality and Secure      1327449600  
27                                          Midi to Midi      1381795200  
28                 Very Heavy Cables At Affordable Price      1341964800  
29                                           Still going      1392768000  
...                                                  ...             ...  
10231                         Cadillac of Guitar Strings      1398902400  
10232  Great for pickin' on the Tele or almost anythi...      1402704000  
10233                               All they claim to be      1400716800  
10234                                       Nice strings      1399766400  
10235                   Great strings! They really sing!      1401494400  
10236  A little skinny for me, but the quality is und...      1400976000  
10237        Noticeable Improvement... but at what cost?      1398643200  
10238                 Nice bright sound with easy action      1403049600  
10239                                Stay In Tune Better      1402963200  
10240                A Good Guitar Deserves Good Strings      1405036800  
10241                                      Great Strings      1403654400  
10242                   These Do Wonders For My Seagull!      1404950400  
10243                          Great tone and less noise      1405123200  
10244              Excellent strings - used on my Taylor      1403740800  
10245  These sound great on some guitars, not so grea...      1404172800  
10246                Great sounding and playing strings!      1405728000  
10247          I'm a BIG Fan of Elixir Strings... But...      1403568000  
10248                    Your Guitar Wants These Strings      1405036800  
10249                                      Great Strings      1403395200  
10250                            Great On A Gibson, Too!      1404950400  
10251                       they sound great, feel great      1404172800  
10252                            Elixirs just sound good      1404086400  
10253    I'm a D'Addario man myself, but hey free is ...      1405900800  
10254                        I really like these strings      1404432000  
10255  Hmmm.... I like them, but with a lot of reserv...      1403654400  
10256                                         Five Stars      1405814400  
10257  Long life, and for some players, a good econom...      1404259200  
10258                                   Good for coated.      1405987200  
10259                                        Taylor Made      1404172800  
10260  These strings are really quite good, but I wou...      1405468800  

[10261 rows x 9 columns]