import json
import pandas as pd
import numpy as np
import csv
import ast


# Reads the original Json file and using the ast library fixes it so python can read it
def readMeta(file):
    try:
        data = []
        with open(file) as meta:
            print('reading: ' + file)
            # generates a map and using ast.literal_eval, python changes the ' into " without breaking the whole strings
            for d in map(ast.literal_eval, meta):
                data.append(d)
                print('fixing json!')
        return data
    except Exception as e:
        print(e)


# Using the metadata that was fixed, generates a new json file with the fixed information, this one can be readed by python.
def exportMeta(data, filename):
    try:
        with open(filename, 'w', encoding='utf-8') as file:
            print('reading: ' + filename)
            for d in data:
                print('Exporting data!')
                file.write(json.dumps(d))
                file.write('\n')
    except Exception as e:
        print(e)


def readJson(jsonFile):
    try:
        data = []
        with open(jsonFile) as file:
            print('reading json!')
            for line in file:
                print('reading data!')
                data.append(json.loads(line))
            return data
    except Exception as e:
        print(e)


def createDataFrame(data, csvFile):
    try:
        print('creating dataframe!')
        df = pd.DataFrame(data)
        df.to_csv(csvFile)
        print('Exporting to csv!')
    except Exception as e:
        print(e)


def exportUsefulData(csvFile):
    try:
        with open(csvFile, 'r', encoding='utf-8') as file:
            reader = csv.DictReader(file)
            asinData = []
            prices = []
            related = []
            salesRank = []
            brand = []
            categories = []
            for row in reader:
                print('Exporting useful data!')
                asinData.append(row['asin'])
                prices.append(row['price'])
                related.append(row['related'])
                salesRank.append(row['salesRank'])
                brand.append(row['brand'])
                categories.append(row['categories'])
            df = pd.DataFrame()
            df['asin'], df['prices'], df['related'], df['salesRank'], df['brand'], df['categories'] = [asinData, prices,
                                                                                                       related,
                                                                                                       salesRank, brand,
                                                                                                       categories]
            print('creating csv!')
            df.to_csv('usefulMetaData.csv')
        file.close()
    except Exception as e:
        print(e)


if __name__ == '__main__':
    file = 'meta_Musical_Instruments.json'
    data = readMeta(file)
    filename = 'meta_Exported.json'
    exportMeta(data, filename)
    exportedData = readJson(filename)
    csvFile = 'metaData.csv'
    createDataFrame(exportedData, csvFile)
    exportUsefulData(csvFile)
