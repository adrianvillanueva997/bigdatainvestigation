import csv
import pandas as pd
import json


def readJson(jsonFile):
    try:
        data = []
        with open(jsonFile) as file:
            print('reading json!')
            for line in file:
                data.append(json.loads(line))
            return data
    except Exception as e:
        print(e)


def getDataFrame(data):
    try:
        df = pd.DataFrame(data)
        with open('dataFrame.txt', 'w', encoding='utf-8') as file:
            print('exportando dataFrame!')
            file.write(str(df))
        return df
    except Exception as e:
        print(e)


def exportUsefulData(dataframe):
    try:
        dataframe.to_csv('export.csv')
        with open('export.csv', 'r', encoding='utf-8') as csvfile:
            print('exportando a csv!')
            reader = csv.DictReader(csvfile)
            asinData = []
            reviewerID = []
            for row in reader:
                print('Exportando asin y reviewerID')
                asinData.append(row['asin'])
                reviewerID.append(row['reviewerID'])
        csvfile.close()
        df = pd.DataFrame(asinData, reviewerID)
        df.to_csv('usefulData.csv')
        print('exportando datos utiles a csv!')
    except Exception as e:
        print(e)


if __name__ == '__main__':
    file = 'Musical_Instruments_5.json'
    data = readJson(file)
    getDataFrame(data)
    df = getDataFrame(data)
    exportUsefulData(df)
